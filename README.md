# opendkim-setter

Generate and maintain DKIM-keys for domains on your setup. This setup relies on the same structure as for [dnssec-signer](see: https://git.giaever.org/joachimmg/dnssec-signer)


# Configuration and installation

Please see the `dnssec-signer`-tool, as this is very similar.

This is a basic configuration that can easily be adapted to suit a standard setup

## Configuration files

Please read them carefully, and read up on OpenDKIM before you continue, so you understand whats going on. This isn't a tutorial.

## FILE: /etc/opendkim.conf
```
# Comments remove, read file you got when installing opendkim

# Log to syslog
Syslog			yes
# For debugging purpose
# LogWhy			true

UMask			002
UserID			opendkim

# Map domains in From addresses to keys used to sign messages
SigningTable		refile:/etc/opendkim/signing.table
KeyTable 		/etc/opendkim/key.table

# Host to ignore when verifying signatures
ExternalIgnoreList	/etc/opendkim/trusted.hosts
InternalHosts		/etc/opendkim/trusted.hosts

# Commonly-used options;
Canonicalization	relaxed/simple
Mode			sv
SubDomains		yes

AutoRestart		yes
AutoRestartRate		10/1M
Background		yes

DNSTimeout		5
SignatureAlgorithm	rsa-sha256

OversignHeaders		From
```

### FILE: /etc/opendkim/signing.table
```
*@domain.tld		domain.tld
*@domain2.tld		domain2.tld
```

### FILE: /etc/opendkim/key.table
```
domain.tld 		domain.tld:DDMMYY:/etc/opendkim/keys/domain.tld/DDMMYY.private
domain2.tld		domain2.tld:DDMMYY:/etc/opendkim/keys/domain2.tld/DDMMYY.private
```

### FILE: /etc/opendkim/trusted.hosts
```
127.0.0.1
::1
localhost
domain.tld
mail.domain.tld
```
Possibly add more domains here, if you are sending through multiple hosts. For my case, every domain sends through `mail.domain.tld`.
